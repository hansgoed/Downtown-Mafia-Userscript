// ==UserScript==
// @name        Downtown mafia automator
// @namespace   Hans Goedegebuure
// @description Cheating Downtown Mafia
// @include     http://www.downtown-mafia.com/*
// @version     1.2
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @grant       GM_getValue
// ==/UserScript==

var UI = {
    init: function () {
        var cheatMenu = $('<div>').addClass('block');
        cheatMenu.append($('<h2>').addClass('title').addClass('cat').text('Cheat'));
        var settingsList = $('<ul>').addClass('nav');

        var pauseItem = this.createMenuItem('pause');
        pauseItem.find('a').click(function () {
            if (UI.getStatus('pause'))
            {
                System.pause();
            }
            else
            {
                System.paused = false;
                var lowestTime = Selector.findLowestTime();

                if (!AntiCheatSolver.detect() && !Selector.getImprisonment())
                {
                    lowestTime.obj.execute();
                    Selector.done = true;
                }
                else
                {
                    if (Selector.getImprisonment())
                    {
                        if (UI.getStatus('SelfBust'))
                        {
                            Crimes.SelfBust.execute();
                            return;
                        }

                        if (Selector.getImprisonmentTime() > 5)
                        {
                            System.timeout = setTimeout(function () { lowestTime.obj.execute(); }, 3000);
                            return;
                        }

                        System.timeout = setTimeout(function () { lowestTime.obj.execute(); }, Selector.getImprisonmentTime() * 1000);
                    }
                }
            }
        });

        pauseItem.prop('id', 'cheat_pauseSetting');
        settingsList.append(pauseItem);

        settingsList.append(this.createMenuItem('Crimes'));
        settingsList.append(this.createMenuItem('Heists'));
        settingsList.append(this.createMenuItem('CarTheft'));
        settingsList.append(this.createMenuItem('CrushCars'));
        settingsList.append(this.createMenuItem('DrugRun'));

        var routesButton = $('<li>');
        var link = $('<a>').text('Drug routes').hover(function () {
            $(this).css('text-decoration', 'underline');
        }, function () {
            $(this).css('text-decoration', 'none');
        }).css('cursor', 'pointer').css('text-align', 'center');
        link.click(function () {
            UI.openRouteSettings();
        });
        routesButton.append(link);

        settingsList.append(routesButton);
        settingsList.append(this.createMenuItem('RacingPractice'));
        settingsList.append(this.createMenuItem('Busting'));
        settingsList.append(this.createMenuItem('SelfBust'));
        settingsList.append(this.createMenuItem('BulletFactory'));
        settingsList.append(this.createMenuItem('ShootingPractice'));
        settingsList.append(this.createMenuItem('IntimidationMission'));
        settingsList.append(this.createMenuItem('CaseWarehouse'));

        cheatMenu.append(settingsList);
        $('#nav').append(cheatMenu);
    },
    createMenuItem: function (setting)
    {
        var item = $('<li>');
        var link = $('<a>').text(this.getLabel(setting)).data('setting', setting);
        link.click(function () { UI.toggleSetting($(this)) });
        var statusLabel = $('<span>').text(this.getStatus(setting) ? 'On' : 'Off').css('float', 'right');
        link.append(statusLabel);
        link.css('cursor', 'pointer');
        item.append(link);

        return item;
    },
    getLabel: function (setting)
    {
        switch (setting)
        {
            case 'CarTheft':
                return 'Car Theft';
            case 'CrushCars':
                return 'Crush stolen cars';
            case 'DrugRun':
                return 'Drug Runs';
            case 'RacingPractice':
                return 'Racing Practice';
            case 'BulletFactory':
                return 'Buy from Bullet Factory';
            case 'IntimidationMission':
                return 'Intimidation shooting';
            case 'ShootingPractice':
                return 'Shooting Practice';
            case 'SelfBust':
                return 'Bust yourself';
            case 'CaseWarehouse':
                return 'The Warehouse Mission';
            case 'pause':
                return 'Pause cheat';
            default:
                return setting;
        }
    },
    getStatus: function (setting) {
        var currentStatus = localStorage['setting_' + setting];
        return (!(currentStatus == null || currentStatus == 0 || !currentStatus));
    },
    toggleSetting: function (menuItem)
    {
        var setting = menuItem.data('setting');
        var currentStatus = localStorage['setting_' + setting];

        if (currentStatus == null || currentStatus == 0 || currentStatus == false)
        {
            localStorage['setting_' + setting] = 1;
        }
        else if (currentStatus == 1)
        {
            localStorage['setting_' + setting] = 0;
        }
        else
        {
            alert('couldn\'t load status of ' + setting + ' setting.');
        }
        var statusLabel = menuItem.find('span');
        statusLabel.text(this.getStatus(setting) ? 'On' : 'Off');
    },
    openRouteSettings: function ()
    {
        if (!System.paused)
        {
            UI.toggleSetting($('#cheat_pauseSetting'));
            System.pause();
        }

        var routesTable = $('<table>').addClass('box').css('width', '75%').prop('id', 'cheat_routesTable');
        var heading = $('<tr>');

        heading.append($('<td>').addClass('top').text('From'));
        heading.append($('<td>').addClass('top').text('To'));
        heading.append($('<td>').addClass('top').text('Drug'));
        heading.append($('<td>').addClass('top').css('width', 46));

        routesTable.append($('<tbody>').append(heading));

        var addButton = $('<button>').addClass('sub').text('Add route').click(UI.addRoute);
        var closeButton = $('<button>').addClass('sub').text('Close').click(function () {
            $('#cheat_routesTable').remove();
            $('#content').children().each(function () {
                $(this).css('height', 'auto');
            })
        });

        var buttonStrip = $('<div>').css('float', 'right').append(addButton).append(closeButton);

        var footer = $('<tfoot>').append($('<tr>').append($('<td>').css('padding', 4).css('color', '#aaa').prop('colspan', 4).append(buttonStrip)));
        routesTable.append(footer);

        var contentDiv = $('#content');

        contentDiv.children().each(function () {
            $(this).css('height', 0);
            $(this).css('overflow', 'hidden');
        });

        contentDiv.prepend(routesTable);

        for (var i = 0; i < Crimes.DrugRun.routes.length; i++)
        {
            UI.addRoute(Crimes.DrugRun.routes[i], routesTable);
        }
    },
    addRoute: function(route, table)
    {
        if (typeof(table) == 'undefined')
        {
            table = $(this).closest('table').find('tbody');
        }

        var countries = CrimesHelper.getCountries();

        var fromSelect = $('<select>').css('width', '100%').addClass('cheat_fromSelect');
        for (var i = 0; i < countries.length; i++)
        {
            fromSelect.append($('<option>').text(countries[i]));
        }
        fromSelect.change(UI.saveRoutes);

        var toSelect = $('<select>').css('width', '100%').addClass('cheat_toSelect');
        for (i = 0; i < countries.length; i++)
        {
            toSelect.append($('<option>').text(countries[i]));
        }
        toSelect.change(UI.saveRoutes);

        var drugs = CrimesHelper.getDrugs();
        var drugSelect = $('<select>').css('width', '100%').addClass('cheat_drugSelect');
        for (i = 0; i < drugs.length; i++)
        {
            drugSelect.append($('<option>').text(drugs[i]));
        }
        drugSelect.change(UI.saveRoutes);

        var removeButton = $('<button>').text('DEL').click(function () {
            $(this).closest('tr').remove();
            UI.saveRoutes();
        });

        var row = $('<tr>').append($('<td>').append(fromSelect)).append($('<td>').append(toSelect)).append($('<td>').append(drugSelect)).append($('<td>').append(removeButton)).addClass('cheat_drugRoute');

        if (typeof(route) != 'undefined')
        {
            fromSelect.val(route.from);
            toSelect.val(route.to);
            drugSelect.val(route.drug);
        }

        table.append(row);
    },
    saveRoutes: function ()
    {
        var routes = [];
        $('.cheat_drugRoute').each(function () {
            var from = $(this).find('.cheat_fromSelect').val();
            var to = $(this).find('.cheat_toSelect').val();
            var drug = $(this).find('.cheat_drugSelect').val();

            routes[routes.length] = { from: from, to: to, drug: drug };
        });

        localStorage['routes'] = JSON.stringify(routes);
    }
};

var System = {
    timeout: 0,
    paused: UI.getStatus('pause'),
    pause: function () {
        clearTimeout(System.timeout);
        System.paused = true;
    }
};

var CrimesHelper = {
    findEasiestCity: function(cityElements)
    {
        //if (window.location.pathname == '/cartheft')
        //{
        //    return $(cityElements[0]); //TODO THIS IS TEMP!
        //}
        //if (window.location.pathname == '/heists')
        //{
        //    return $(cityElements[1]); // TODO THIS IS TEMP!
        //}

        var easiest = $(cityElements[0]);
        for (var i = 1; i < cityElements.length; i++)
        {
            if (this.findHardnessIndex($(cityElements[i])) < this.findHardnessIndex(easiest))
            {
                easiest = $(cityElements[i]);
            }
        }

        return easiest;
    },
    findHardnessIndex: function(cityElement)
    {
        var hardnessPossibilities = [
            'http://caviar.dtmcdn.com/static/images/game/gta/a_0.png',
            'http://caviar.dtmcdn.com/static/images/game/gta/a_1.png',
            'http://caviar.dtmcdn.com/static/images/game/gta/a_2.png',
            'http://caviar.dtmcdn.com/static/images/game/gta/a_3.png',
            'http://caviar.dtmcdn.com/static/images/game/gta/a_4.png'
        ];

        var hardnessLabel = cityElement.find('img');
        var src = hardnessLabel.prop('src');

        var hardnessIndex = hardnessPossibilities.indexOf(src);
        if (hardnessIndex >= 0)
        {
            return hardnessIndex;
        }
        else
        {
            alert('Anomaly found when checking for the right city to click');
        }
    },
    submitForm: function ()
    {
        var paused = UI.getStatus('pause');
        if (paused)
        {
            return;
        }

        var submitButton = $('form button.sub');
        if (submitButton.length == 0)
        {
            submitButton = $('form input.sub');
        }
        if (submitButton.length > 1)
        {
            alert('Anomaly detected when checking for multiple submit buttons');
            return;
        }

        var countdown = $('#countdown');
        if (submitButton.first().is(':enabled') && countdown.length == 0)
        {
            this.click(submitButton);
        }
        else
        {
            if (countdown.length > 0)
            {
                System.timeout = setTimeout(function () {
                    if (!CrimesHelper.click(submitButton))
                    {
                        alert('Anomaly detected when checking for submit button label values');
                    }
                    var interval = setInterval(function () {
                        if (!CrimesHelper.click(submitButton))
                        {
                            clearInterval(interval);
                            alert('Anomaly detected when checking for submit button label values'); }
                    }, 500);
                }, (Selector.parseTime(countdown.text()) * 1000) - 1);
            }
            else
            {
                console.log('darn');
                setInterval(function() { submitButton.first().click() }, 1000);
            }
        }
    },
    click: function (submitButton)
    {
        if (submitButton == null)
        {
            alert('couldn\'t find submit button');
        }
        var possibleTexts = [
            'Execute GTA',
            'Commit',
            'Execute Heist',
            'Practice!',
            'Travel to America!',
            'Travel to Brazil!',
            'Travel to England!',
            'Travel to China!',
            'Travel to South-Africa!',
            'Travel to Japan!',
            'Intimidate AndrewRusso!',
            'Enter Range!',
            'Case the WarehouseCurrent Time of Day: Morning',
            'Case the WarehouseCurrent Time of Day: Day',
            'Case the WarehouseCurrent Time of Day: Afternoon',
            'Case the WarehouseCurrent Time of Day: Night'
        ];

        var label = submitButton.text();
        if (label == "")
        {
            label = submitButton.val();
        }

        if (possibleTexts.indexOf(label) >= 0)
        {
            submitButton.click();
        }
        else
        {
            return false;
        }
        return true;
    },
    getLocation: function () {
        return $('li.ico.location .value.ajaxCountry').first().text().replace(/-|\s/g, ' ');
    },
    getCountries: function () {
        return [
            'America',
            'Brazil',
            'England',
            'China',
            'South Africa',
            'Japan'
        ];
    },
    getDrugs: function () {
        return [
            'Marijuana',
            'Heroin',
            'Morphine',
            'Cocaine',
            'Opium'
        ];
    }
};

UI.init();

// 1 is on
// 0 is off
var CrimeSettings = {
    Crimes: UI.getStatus('Crimes'),
    Heists: UI.getStatus('Heists'),
    CarTheft: UI.getStatus('CarTheft'),
    CrushCars: UI.getStatus('CrushCars'),
    DrugRun: UI.getStatus('DrugRun'),
    RacingPractice: UI.getStatus('RacingPractice'),
    Busting: UI.getStatus('Busting'),
    BulletFactory: UI.getStatus('BulletFactory'),
    ShootingPractice: UI.getStatus('ShootingPractice'),
    IntimidationMission: UI.getStatus('IntimidationMission'),
    SelfBust: UI.getStatus('SelfBust'),
    CaseWarehouse: UI.getStatus('CaseWarehouse')
};

$(document).ready(function () {
    if (System.paused)
    {
        return;
    }

    ActionResultHandler.getResult();
    RewardChecker.check();

    var lowestTime = Selector.findLowestTime();

    if (!AntiCheatSolver.detect() && !Selector.getImprisonment())
    {
        lowestTime.obj.execute();
        this.done = true;
    }
    else
    {
        if (Selector.getImprisonment())
        {
            if (UI.getStatus('SelfBust'))
            {
                Crimes.SelfBust.execute();
                return;
            }

            if (Selector.getImprisonmentTime() > 5)
            {
                System.timeout = setTimeout(function () { lowestTime.obj.execute(); }, 3000);
                return;
            }

            System.timeout = setTimeout(function () { lowestTime.obj.execute(); }, Selector.getImprisonmentTime() * 1000);
        }
    }
});

var Selector = {
    done: false,
    findLowestTime: function () {
        var times = this.getTimes();
        var lowestTime = times[0];
        for (var i = 1; i < times.length; i++)
        {
            if (lowestTime.time > times[i].time)
            {
                lowestTime = times[i];
            }
        }

        return lowestTime;
    },
    getTimes: function ()
    {
        var times = [];
        var crimes = [];

        $.each(Crimes, function ()
        {
            crimes[crimes.length] = this
        });

        for (var i = 0; i < crimes.length; i++)
        {
            if (crimes[i].isEnabled())
            {
                times[times.length] = new Time(crimes[i].getTime(), crimes[i]);
            }
        }

        return times;
    },
    getTime: function (menuItemElement)
    {
        var timeElement = menuItemElement.find('span');
        var time = 0;
        if (timeElement.length > 0)
        {
            time = this.parseTime(timeElement.text());
        }
        return time
    },
    parseTime: function (time)
    {
        if (time == "")
        {
            return 0;
        }

        var timeArray = time.split(':');
        var seconds = 0;
        seconds += parseInt(timeArray[0]) * 3600;
        seconds += parseInt(timeArray[1]) * 60;
        seconds += parseInt(timeArray[2]);

        return seconds;
    },
    getImprisonment: function () {
        return (this.getImprisonmentTime() > 0);
    },
    getImprisonmentTime: function() {
        return Selector.getTime($('#menuJail'));
    }
};

var Crimes = {
    Crimes: {
        isEnabled: function () {
            return CrimeSettings.Crimes
        },
        getTime: function ()
        {
            return Selector.getTime($('#menuCrimes'));
        },
        execute: function ()
        {
            if (window.location.pathname != '/crimes')
            {
                $('#menuCrimes').find('a')[0].click();
                return;
            }

            var possibleCrimes = [
                'Break into the bank vault at night',
                'Defraud the government tax agency',
                'Hijack a money transport vehicle',
                'Steal contraband from Police Headquarters',
                'Blackmail the owner of an airline',
                'Complete a hit on a mafioso',
                'Threaten the King Brotherhood\'s chemist',
                'Pilfer a shopping complex at night',
                'Kidnap a child for ransom',
                'Hold up your local corner shop',
                'Search a wrecked car for brass',
                'Pickpocket a local businessman'
            ];

            var form = $('form');
            var label = form.find('label:visible:first');
            var regex = /^\d{1,2}%$/;

            // Steal contraband mission
            var mission1Label = form.find('label:visible:contains("Steal contraband from Police Headquarters")');
            if (mission1Label.length == 1)
            {
                var suspicion = $('table.box').find('div').filter(function () {
                    return regex.test($(this).text());
                });

                var countries = [];

                if (typeof(localStorage['recoveredContraband']) != 'undefined')
                {
                    countries = JSON.parse(localStorage['recoveredContraband']);
                }

                if (parseInt(suspicion.text().replace('%', '')) >= 2 || countries.indexOf(CrimesHelper.getLocation()) >= 0)
                {
                    if (label.text() == 'Steal contraband from Police Headquarters')
                    {
                        label = form.find('label:visible:eq(1)');
                        if (label.text() == 'Steal contraband from Police Headquarters')
                        {
                            alert('Selected second input, but still doing contraband mission');
                            return;
                        }
                    }
                }
                else
                {
                    if (isNaN(parseInt(suspicion.text().replace('%', ''))))
                    {
                        alert("Police suspicion translated to Not a Number");
                    }
                }
            }

            // Fear scientist mission
            var mission2Label = form.find('label:visible:contains("Threaten the King Brotherhood\'s chemist")');
            if (mission2Label.length == 1)
            {
                var fear = $('table.box').find('div').filter(function () {
                    return regex.test($(this).text());
                });

                if (parseInt(fear.text().replace('%', '')) >= 2)
                {
                    if (label.text() == 'Threaten the King Brotherhood\'s chemist')
                    {
                        label = form.find('label:visible:eq(1)');
                        if (label.text() == 'Threaten the King Brotherhood\'s chemist')
                        {
                            alert('Selected second input, but still doing King Brotherhood mission');
                            return;
                        }
                    }
                }
                else
                {
                    if (isNaN(parseInt(fear.text().replace('%', ''))))
                    {
                        alert("King Brotherhood chemist fear translated to Not a Number");
                    }
                }
            }

            // Normal service resumed
            if (possibleCrimes.indexOf(label.text()) >= 0)
            {
                if (!$('#' + label.prop('for')).is(':checked'))
                {
                    label.click();
                }
            }
            else
            {
                alert('Anomaly found when checking crime labels');
                return;
            }
            CrimesHelper.submitForm();
        }
    },
    Heists: {
        isEnabled: function () {
            return CrimeSettings.Heists;
        },
        getTime: function () {
            return Selector.getTime($('#menuHeists'));
        },
        execute: function () {
            if (window.location.pathname != '/heists')
            {
                $('#menuHeists').find('a')[0].click();
                return;
            }

            var cities = $('form .cities');
            var easiestCity = CrimesHelper.findEasiestCity(cities);

            // Assumption: When easiest city has been selected area is also selected
            if (!easiestCity.hasClass('isSelected'))
            {
                easiestCity.click();

                var allAreas = ['Commercial', 'Residential', 'Industrial'];
                var areas = $('button.areas');

                var failed = false;
                areas.each(function ()
                {
                    if (allAreas.indexOf($(this).text()) < 0)
                    {
                        failed = true;
                        return false;
                    }
                });
                if (failed)
                {
                    alert('Anomaly found when checking the areas');
                    return;
                }

                var rand = Math.random();
                $(areas[Math.floor(rand * allAreas.length)]).click();
            }

            CrimesHelper.submitForm();
        }
    },
    CarTheft: {
        isEnabled: function () {
            return CrimeSettings.CarTheft;
        },
        getTime: function () {
            return Selector.getTime($('#menuCarTheft'));
        },
        execute: function () {
            if (window.location.pathname != '/cartheft')
            {
                $('#menuCarTheft').find('a')[0].click();
                return;
            }

            if (CrimeSettings.CrushCars)
            {
                var rows = $('#refreshLand').find('table.box').find('tr:not(:nth-child(1)):not(:nth-child(2))');
                var submit = false;
                rows.each(function () {
                    var wantedImages = $(this).find('td:last-child').find('img');

                    var wantedLevel = 0;
                    wantedImages.each(function ()
                    {
                        //console.log($(this));
                        if ($(this).css('opacity') == 1)
                        {
                            wantedLevel++;
                        }
                    });
                    if (wantedLevel >= 2)
                    {
                        $(this).find('td:first-child').find('input:visible:checkbox').click();
                        submit = true;
                    }
                });

                if (submit)
                {
                    $('#act_crush').click();
                    setInterval(function () {
                        var acceptButton = $('#act_crush_accept');
                        if (acceptButton.length > 0)
                        {
                            acceptButton.click();
                            setInterval(function () {
                                if ($('.act_refresh').length > 0) {
                                    window.location = window.location.href;
                                }
                            }, 500);
                        }
                    }, 500);
                    return;
                }
            }

            var cities = $('#gta1').find('.cities:visible');
            if (cities.length == 0)
            {
                $('#menuCarTheft').find('a')[0].click();
                return;
            }

            var easiestCity = CrimesHelper.findEasiestCity(cities);

            if (!easiestCity.hasClass('isSelected'))
            {
                easiestCity.click();
            }

            CrimesHelper.submitForm();
        }
    },
    DrugRun: {
        isEnabled: function () {
            return CrimeSettings.DrugRun;
        },
        getTime: function () {
            //return 0;
            var time = Selector.getTime($('#menuAirport'));
            if (time == 0 || localStorage['fly_location'] == CrimesHelper.getLocation())
            {
                return -1;
            }
            return time;
        },
        execute: function () {
            if (window.location.pathname != '/druglab' && window.location.pathname != '/drugs' && window.location.pathname != '/airport')
            {
                $('#menuDrugLab').find('a')[0].click();
                return;
            }

            if (window.location.pathname == '/druglab')
            {
                $('div.tabsContainer a.tab:contains("Drug Market"):visible')[0].click();
                return;
            }

            var location = CrimesHelper.getLocation();
            if (window.location.pathname == '/drugs')
            {
                for (var i = 0; i < this.routes.length; i++)
                {
                    if (location != this.routes[i].from)
                    {
                        continue;
                    }

                    var amountDiv = $('#content').find('div').filter(function () {
                        var regExp = /^Current Stock: \d{1,3},?\d{0,3} unitsMaximum Capacity: \d{1,3},?\d{0,3} units$/;
                        return regExp.exec($(this).text());
                    });
                    var amounts = amountDiv.text().split('units');
                    if (amounts.length != 3)
                    {
                        alert('Anomaly detected when trying to find current and maximum stock');
                        return;
                    }
                    var currentStock = amounts[0].replace(/\D/g, '');
                    var maxStock = amounts[1].replace(/\D/g, '');

                    // Check if everything is ready for shipping
                    if (currentStock > 0)
                    {
                        var stockOfDrugToBuy = parseInt($(this.drugInputs[this.routes[i].drug]).parent().text().replace(/\d+\s+$/, '').replace(/\D/g, ''));
                        if (stockOfDrugToBuy == maxStock)
                        {
                            localStorage['fly_location'] = this.routes[i].to;

                            $('#menuAirport').find('a')[0].click();
                            return;
                        }
                        else
                        {
                            // Sell everything, we don't have the drugs we need
                            $('#selectAll').click();
                            $('input[name=sell]').click();
                            return;
                        }
                    }

                    localStorage['fly_location'] = null;

                    // If not, buy the max possible
                    this.drugInputs[this.routes[i].drug].val(maxStock);
                    $('input[name=buy]').click();

                    // Don't use submitform for this one, this one's different (multiple submit buttons)
                    return;
                }

                alert('No route found for this country!');
                // TODO disable drug running for this country
            }
            if (window.location.pathname == '/airport')
            {
                if (localStorage['fly_location'] != location)
                {
                    var countryDiv = this.countryDivs[localStorage['fly_location'].replace(/-|\s/g, '')];
                    if (!countryDiv.hasClass('isSelected'))
                    {
                        countryDiv.click();
                    }
                    CrimesHelper.submitForm();
                }
            }
        },
        routes: (typeof(localStorage['routes']) == 'undefined') ? [] : JSON.parse(localStorage['routes']),
        drugInputs: {
            Marijuana: $('#sa'),
            Heroin: $('#sb'),
            Morphine: $('#sc'),
            Cocaine: $('#sd'),
            Opium: $('#se')
        },
        countryDivs: {
            America: $('#city1'),
            Brazil: $('#city2'),
            England: $('#city3'),
            China: $('#city4'),
            SouthAfrica: $('#city5'),
            Japan: $('#city6')
        }
    },
    RacingPractice: {
        isEnabled: function () {
            return CrimeSettings.RacingPractice;
        },
        getTime: function ()
        {
            return Selector.getTime($('#menuRacing'));
        },
        execute: function () {
            // Navigation stuff
            if (window.location.pathname != '/racing' && window.location.pathname + window.location.search != '/racing.php?id=2')
            {
                $('#menuRacing').find('a')[0].click();
                return;
            }
            if (window.location.pathname + window.location.search != '/racing.php?id=2')
            {
                var link = $('table.box').find('a:contains("Practice"):visible');
                window.location.href = link.prop('href'); // I'd rather use click, but that doesn't seem to work for some reason
                return;
            }

            // Clicky stuff
            var carSelect = $('select[name="carselect"]:visible');
            if (carSelect.length == 0)
            {
                alert('You have no cars here!');
                // TODO disable racing practice for this country
                return;
            }

            var allDisciplines = [
                'Sprint',
                'Drag',
                'Drift'
            ];
            var disciplineSelect = $('select[name="discipline"]:visible');
            var disciplines = disciplineSelect.find('option');
            disciplines.each(function () {
                if (allDisciplines.indexOf($.trim($(this).text())) < 0)
                {
                    alert('Anomaly detected when checking for discipline labels');
                    return false;
                }
            });

            disciplineSelect.click();
            disciplines[Math.floor(Math.random() * disciplines.length)].click();

            setTimeout(function () {
                CrimesHelper.submitForm();
            }, this.getTime() * 1000);
        }
    },
    Busting: {
        isEnabled: function () {
            return CrimeSettings.Busting;
        },
        crewPriority: false,
        getTime: function () {
            if ($('a.payroll.perkPop').hasClass('perkOff'))
            {
                return 60; // Busting sentence seems to be 80 seconds; Don't let busting stand in the way of normal crimes
                //TODO return 80 or custom value perhaps?
            }
            else
            {
                return 80 * 0.2;
            }
        },
        execute: function () {
            if (window.location.pathname != '/jail')
            {
                $('#menuJail').find('a')[0].click();
                return;
            }

            if (Selector.getImprisonment() && !AntiCheatSolver.detect())
            {
                $('#menuJail').find('a')[0].click();
                return;
            }

            // Find best target to bust
            var inmateRows = $('#content').find('table').first().find('tr:not(:first-child)').filter(function () {
                return $(this).find('td:first').css('background-color') != 'rgb(32, 62, 66)';
            });
            if (inmateRows.length == 0)
            {
                $('#menuJail').find('a')[0].click();
                return;
            }

            var vipInmate = [];
            // Bust missions & crew members
            if (this.crewPriority)
            {
                vipInmate = inmateRows.find('img').filter(function () {
                    return ($(this).prop('src') == 'http://caviar.dtmcdn.com/render/4387');
                });
            }

            var easiest = new Bust(999, null);
            if (vipInmate.length == 1)
            {
                easiest = new Bust(-1, vipInmate.closest('tr')); // TODO, must have made this drunk or tired.. Multiple results can occur..
            }

            inmateRows.each(function () {
                var hardness = Crimes.Busting.getHardnessIndex($(this).find('td:first').find('img').prop('src'));
                if (hardness < easiest.hardness)
                {
                    easiest = new Bust(hardness, $(this));
                }
                if (easiest.element == null)
                {
                    setTimeout(function () {
                        $('#menuJail').find('a')[0].click();
                    }, 1000);
                    return;
                }
                easiest.element.find('button.sub').click();
            });
        },
        getHardnessIndex: function (image) {
            var images = [
                'http://caviar.dtmcdn.com/render/37388/28',
                'http://caviar.dtmcdn.com/render/37389/28',
                'http://caviar.dtmcdn.com/render/37390/28'
            ];

            var hardness = images.indexOf(image);
            if (hardness < 0)
            {
                if (typeof(image) !== 'undefined')
                {
                    alert('Failed to establish hardness of busting');
                }
                return 999;
            }
            return hardness;
        }
    },
    BulletFactory: {
        isEnabled: function () {
            return CrimeSettings.BulletFactory;
        },
        getTime: function () {
            var location = CrimesHelper.getLocation();
            if (typeof localStorage['bulletFactory'] == 'undefined' || localStorage['bulletFactoryLocation'] != location)
            {
                return 0;
            }
            else
            {
                return (localStorage['bulletFactory'] - new Date().getTime()) / 1000;
            }
        },
        execute: function () {
            if (window.location.pathname != '/bullets')
            {
                $('#menuBF').find('a')[0].click();
                return;
            }

            var nextRelease = $('table.box td div:contains("NEXT RELEASE:")');
            if (nextRelease.length == 0)
            {
                alert('couldn\'t establish next release');
                return;
            }
            var text = $(nextRelease).text().replace(/NEXT RELEASE:/g, '');

            var stock = $('table.box td div:contains("STOCK:")');
            if (stock.length == 0)
            {
                alert('couldn\'t establish stock');
                return;
            }

            var location = CrimesHelper.getLocation();

            var stockText = $(stock).text().replace(/STOCK:/g, '');

            var br = false;
            if (parseInt(stockText) > 0)
            {
                localStorage['bulletFactory'] = 1;
                localStorage['bulletFactoryLocation'] = location;
                var bulletForm = $('#bulletForm');
                if (bulletForm.find('input:visible').length == 1)
                {
                    $('#maxImg').click();
                    $('#bulletPurchase').click();
                }
                else
                {
                    $('body').css('background-image', 'none').css('background', '#ff0000');
                    setTimeout(function () {
                        window.location = window.location.href;
                    }, 60000);
                }
            }
            else
            {
                switch (text) {
                    case '1 hour':
                        localStorage['bulletFactory'] = new Date().getTime() + 50 * 60 * 1000;
                        localStorage['bulletFactoryLocation'] = location;
                        break;
                    case '30-50 minutes':
                        localStorage['bulletFactory'] = new Date().getTime() + 30 * 60 * 1000;
                        localStorage['bulletFactoryLocation'] = location;
                        break;
                    case '15-30 minutes':
                        localStorage['bulletFactory'] = new Date().getTime() + 15 * 60 * 1000;
                        localStorage['bulletFactoryLocation'] = location;
                        break;
                    case '5-15 minutes':
                        localStorage['bulletFactory'] = new Date().getTime() + 5 * 60 * 1000;
                        localStorage['bulletFactoryLocation'] = location;
                        break;
                    case '<5 minutes':
                        localStorage['bulletFactory'] = new Date().getTime() + 60 * 1000;
                        localStorage['bulletFactoryLocation'] = location;
                        break;
                    case '1 minute':
                        localStorage['bulletFactory'] = 1;
                        localStorage['bulletFactoryLocation'] = location;
                        setTimeout(function () {
                            window.location = window.location.href;
                        }, 2000);
                        br = true;
                        return;
                    default:
                        var time = Selector.parseTime(text);
                        if (time == 0)
                        {
                            alert('Couldn\'t establish next release ("' + text + '")');
                            break;
                        }
                        else
                        {
                            localStorage['bulletFactory'] = new Date().getTime() + time * 1000;
                            localStorage['bulletFactoryLocation'] = location;
                        }
                        break;
                }

                if (!br)
                {
                    var lowestTime = Selector.findLowestTime();

                    if (lowestTime.time < this.getTime())
                    {
                        lowestTime.obj.execute();
                    }
                    else
                    {
                        setTimeout(function () {
                            window.location = window.location.href;
                        }, this.getTime() * 1000);
                    }
                }
            }
        }
    },
    IntimidationMission: {
        isEnabled: function () {
            return CrimeSettings.IntimidationMission;
        },
        getTime: function () {
            if (localStorage['setting_IntimidationMission'] != 1)
            {
                return 99999; //HACK
            }

            if (localStorage['time_intimidationMission'] == null)
            {
                return 0;
            }

            return localStorage['time_intimidationMission'] - (new Date().getTime() / 1000);
        },
        execute: function () {
            // Navigation
            if (window.location.pathname != '/kill' && window.location.pathname + window.location.search != '/kill?id=6')
            {
                $('#menuKill').find('a')[0].click();
                return;
            }
            if (window.location.pathname + window.location.search != '/kill?id=6')
            {
                var link = $('div.tabsContainer').find('a:contains("Intimidate"):visible');
                if (link.length == 0)
                {
                    localStorage['setting_IntimidationMission'] = 0;

                    lowestTime = Selector.findLowestTime();
                    lowestTime.obj.execute();
                    return;
                }
                window.location.href = link.prop('href'); // I'd rather use click, but that doesn't seem to work for some reason
                return;
            }

            var regex = /^You cannot intimidate yet. Wait (\d{2}:\d{2}:\d{2})\.$/;
            var timeElement = $('form span').filter(function () {
                return regex.test($(this).text());
            });

            if (timeElement.length == 0)
            {
                // We are ready
                $('select[name=intimidate_bullets]').find('option').last().prop('selected', true);
                $('select[name=intimidate_attack]').find('option').last().prop('selected', true);

                CrimesHelper.submitForm();
            }
            else
            {
                if (timeElement.length == 1)
                {
                    var matches = timeElement.text().toString().match(regex);
                    console.log(Selector.parseTime(matches[1]));
                    localStorage['time_intimidationMission'] = (new Date().getTime() / 1000) + Selector.parseTime(matches[1]);
                    var lowestTime = Selector.findLowestTime();

                    if (lowestTime.time < this.getTime())
                    {
                        lowestTime.obj.execute();
                    }
                    else
                    {
                        setTimeout(function () {
                            window.location = window.location.href;
                        }, this.getTime() * 1000);
                    }
                }
                else
                {
                    alert('Anomaly found while checking intimidation time');
                }
            }
        }
    },
    ShootingPractice: {
        isEnabled: function () {
            return CrimeSettings.ShootingPractice;
        },
        getTime: function () {
            if (localStorage['time_shootingPractice'] == null)
            {
                return 0;
            }

            return localStorage['time_shootingPractice'] - (new Date().getTime() / 1000);
        },
        execute: function () {
            if (window.location.pathname != '/kill' && window.location.pathname != '/shootingrange')
            {
                $('#menuKill').find('a')[0].click();
                return;
            }

            if (window.location.pathname != '/shootingrange')
            {
                var link = $('div.tabsContainer').find('a:contains("Shooting Range"):visible');
                if (link.length == 0)
                {
                    alert('Anomaly found while checking for Shooting Range button');
                    return;
                }
                window.location.href = link.prop('href'); // I'd rather use click, but that doesn't seem to work for some reason
                return;
            }

            var regex = /^You must wait (\d{2}:\d{2}:\d{2}) to enter the Shooting Range again!|You have entered the Shooting Range!You may enter again in (\d{2}:\d{2}:\d{2})$/
            var matches = $('#theBox').text().toString().match(regex);

            if (matches != null)
            {

                if (typeof(matches[1]) != 'undefined')
                {
                    localStorage['time_shootingPractice'] = (new Date().getTime() / 1000) + Selector.parseTime(matches[1]);
                }

                if (typeof(matches[2]) != 'undefined')
                {
                    localStorage['time_shootingPractice'] = (new Date().getTime() / 1000) + Selector.parseTime(matches[2]);
                }

                var lowestTime = Selector.findLowestTime();

                if (lowestTime.time < this.getTime())
                {
                    lowestTime.obj.execute();
                }
            }

            $('input[name=bulletamount]:visible').val('1000');
            CrimesHelper.submitForm();
        }
    },
    SelfBust: {
        isEnabled: function () {
            return false; // Dirty hack. Only using execute function of this object
        },
        getTime: function () {
            return 0;
        },
        execute: function () {
            if (window.location.pathname != '/jail')
            {
                $('#menuJail').find('a')[0].click();
                return;
            }

            // Find yourself
            var inmateRow = $('#content').find('table').first().find('tr:not(:first-child)').filter(function () {
                return $(this).find('td:first').css('background-color') == 'rgb(32, 62, 66)';
            });

            if (inmateRow.length != 1)
            {
                alert('Anomaly found while trying to find yourself in jail');
            }

            var hardness = Crimes.Busting.getHardnessIndex(inmateRow.find('td:first').find('img').prop('src'));
            if (hardness == 0)
            {
                inmateRow.find('button.sub').click();
                return;
            }

            var lowestTime = Selector.findLowestTime();

            if (Selector.getImprisonmentTime() > 5)
            {
                System.timeout = setTimeout(function () { lowestTime.obj.execute(); }, 3000);
                return;
            }

            System.timeout = setTimeout(function () { lowestTime.obj.execute(); }, Selector.getImprisonmentTime() * 1000);
        }
    },
    CaseWarehouse: {
        isEnabled: function () {
            return CrimeSettings.CaseWarehouse && CrimesHelper.getLocation() == 'South Africa';
        },
        getTime: function () {
            if (localStorage['time_caseWarehouseMission'] == null)
            {
                return 0;
            }
            return localStorage['time_caseWarehouseMission'] - (new Date().getTime() / 1000);
        },
        execute: function () {
            visitMissionPage = localStorage['caseWarehouseMission_visitMissionPage'];
            if (visitMissionPage == null)
            {
                visitMissionPage = true;
            }
            
            if (window.location.pathname != '/missions' && visitMissionPage == null )
            {
                $('#menuMissions').find('a')[0].click();
                return;
            }

            var regex = /^You must rest (\d{2}:\d{2}:\d{2}) before casing again.|You must wait (\d{2}:\d{2}:\d{2}) to case the warehouse again.$/
            var matches = $('#theBox').text().toString().match(regex);

            if (matches != null)
            {
                if (typeof(matches[1]) != 'undefined')
                {
                    localStorage['time_caseWarehouseMission'] = (new Date().getTime() / 1000) + Selector.parseTime(matches[1]);
                }

                if (typeof(matches[2]) != 'undefined')
                {
                    localStorage['time_caseWarehouseMission'] = (new Date().getTime() / 1000) + Selector.parseTime(matches[2]);
                }
            }

            var submitButton = $('form button.sub');

            regex = /^Case the WarehouseCurrent Time of Day: (Morning|Day|Afternoon|Night)$/

            matches = submitButton.text().toString().match(regex);

            if (matches == null)
            {
                alert('Anomaly found while checking for time of day');
            }

            // If this time of day is already finished check again in 5 minutes
            var row = $('table.box').find('tbody').find('tr').filter(function () {
                return $(this).find('td:first').text() == 'Morning';//matches[1];
            });

            if (row.length == 0)
            {
                localStorage['setting_CaseWarehouse'] = 0;
            }
            console.log(row.html());

            if (row.find('td:last').find('img').prop('src') != location.protocol + '//' + location.hostname + '/static/icons/cross.png')
            {
                if (this.getTime() < 300)
                {
                    localStorage['time_caseWarehouseMission'] = (new Date().getTime() / 1000) + 300;
                }
            }

            var lowestTime = Selector.findLowestTime();

            if (lowestTime.time < this.getTime())
            {
                lowestTime.obj.execute();
            }

            CrimesHelper.submitForm();
        }
    }
};

var AntiCheatSolver = {
    detect: function ()
    {
        // Detect V1
        return $('#head').text() == 'Script Protection';
    }
};

var ActionResultHandler = {
    getResult: function () {
        var result = $('#theBox');
        if (result.length == 1)
        {
            if (result.text() == 'You already have all required contraband in this country.Move on to the next country.')
            {
                var countries = [];

                if (typeof(localStorage['recoveredContraband']) != 'undefined')
                {
                    countries = JSON.parse(localStorage['recoveredContraband']);
                }

                countries[countries.length] = CrimesHelper.getLocation();

                localStorage['recoveredContraband'] = JSON.stringify(countries);
            }
        }
    }
};

var RewardChecker = {
    check: function () {
        var rewardsBox = $('#rewardsBox');
        if (rewardsBox.length > 0)
        {
            var rewards = rewardsBox.find('.rewards');
            var random = Math.ceil(Math.random() * rewards.length) - 1;
            rewards[random].click();
        }
    }
};

function Time(time, obj)
{
    this.time = time;
    this.obj = obj;
}

function Bust(hardness, element)
{
    this.hardness = hardness;
    this.element = element;
}